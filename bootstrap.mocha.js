global.ENDPOINT = 'http://local.test/api';

global._ = require('lodash');
global.getMW = function(name, module ) {
  this. options = {
    endpoint : global.ENDPOINT,
    token : '12345678',
    timeout : 30000,
    version : '0.0.1'
  };
  MW = function(options) {
    if (!(this instanceof MW)) { return new MW(options) }
    this.options = {
      endpoint : global.ENDPOINT,
      token : '12345678',
      timeout : 30000,
      version : '0.0.1'
    };
  };
  MW.prototype.__request = require('./src/lib/request.js');
  MW.prototype[name] = module;
  return MW();
};

