# Bolingo.io : Home Kontrol Javascript SDK

![alt text](https://codeship.com/projects/622a14e0-b490-0134-4ce6-665c05a5a8f8/status?branch=master "Codeship")

The Home Kontrol JS SDK simplifies interacting with the Bolingo Smart Home API.

## Installation

```
 $ npm install https://olivierodo@bitbucket.org/olivierodo/bolingo-node-mw.git
```

## Basic Usage

### Node

```javascript
  const Bolingo = require('bolingo-node-mw')
  const client = Bolingo({
    endpoint : '[Api enpoint]',
    token : '[API token]',
  });

  client.time().then( el => {
    console.log (el.time);
  });
```

### Options

The options can be pass to the function or defined as environment variables :

- `endpoint` to define the api end point url (also `MW_URL`  env variable)
- `token` to define the api token  (also `MW_TOKEN`  env variable)
- `timeout` to define the api timeout call (also `MW_TIMEOUT`  env variable)

### Features

#### Time

##### Retrive the current time

```javascript
  client.time().then( el => {
    console.log (el);
  });
```
#### Weather

##### Retrive the weather

```javascript
  client.weather('2017-01-01').then( el => {
    console.log (el);
  });
```

#### Status

##### Retrive the smart home status

```javascript
  client.status().then( el => {
    console.log (el);
  });
```

#### Fish

##### Feed the fish

```javascript
  client.fish().feed().then( el => {
    console.log (el);
  });
```

##### Get the fish activity

```javascript
  client.fish().get().then( el => {
    console.log (el);
  });
```

#### Note

##### Retrive the list of the notes

```javascript
  client.note().get().then( el => {
    console.log (el);
  });
```

##### Create a new note

```javascript
  const data = {
    title : 'test',
  };
  client.note().create(data).then( el => {
    console.log (el);
  });
```

#### Bot

##### Talk to the bot intelligence

```javascript
  client.bot().talk('<my text>', 'user id', additionnal_data_object).then( el => {
    console.log (el);
  });
```

#### Device

##### Retrive the list of devices

```javascript
  client.device().get().then( el => {
    console.log (el);
  });
```

##### Create a new device 

```javascript
  const data = {
    name : 'test',
    ip : '1.1.1.1'
  };
  client.device().create(data).then( el => {
    console.log (el);
  });
```

##### Update a device

```javascript
  const id = 123;
  const data = {
    name : 'test',
    ip : '1.1.1.1'
  };
  client.device().update(id, data).then( el => {
    console.log (el);
  });
```

#### Pictures

##### Retrive the list of the pictures

```javascript
  client.pictures().get().then( el => {
    console.log (el);
  });
```

##### Add a picture

```javascript
  var picture = {
    base64 : 'rererererewewwqqs',
    location : 'eerred'
    info...
  };
  client.pictures().create(picture).then( el => {
    console.log (el);
  });
```


#### Todo

##### Retrive the list of todo list

```javascript
  client.todo().get().then( el => {
    console.log (el);
  });
```

##### Get the detail of a todo list

```javascript
  client.todo().get(12).then( el => {
    console.log (el);
  });
```

##### Add task to a todo list

```javascript
  const tasks = [{
    title : 'task1'
  },{
    title : 'task2'
  }];
  client.todo().addTasks(12, tasks).then( el => {
    console.log (el);
  });
```


## Development


### Run test

```
$ npm test
```

## Debug

Use the environment variable

```
NODE_DEBUG=mw
```