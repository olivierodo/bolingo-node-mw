var MW =  require('./index.js'),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');


describe('Index', () => {

  it('check options', (done) => {
      var options = {
        endpoint : global.ENDPOINT,
        token : '1234567890',
        timeout : 3000,
        version : require('./package.json').version
      };

      var mw = MW(options);

      expect(mw.options).to.deep.equal(options);
      done();
  });

  it('check models', (done) => {
    expect(MW).to.respondTo('__request');
    expect(MW).to.respondTo('status');
    expect(MW).to.respondTo('time');
    expect(MW).to.respondTo('weather');
    expect(MW).to.respondTo('note');
    expect(MW).to.respondTo('todo');
    expect(MW).to.respondTo('device');
    expect(MW).to.respondTo('fish');
    done();
  })
});




