const MW =  global.getMW(),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');


describe('Request', function() {

  request = MW.__request;

  describe('#get()', function() {

    var sandbox;

    beforeEach(function(done) {
      sandbox = sinon.sandbox.create();
      done();
    });

    afterEach(function(done) {
      sandbox.restore();
      done();
    });

    it('Should call _request with the good parameters when there is no parameters', function(done) {
        var client = request('travel/country');
        sandbox.stub(client, '_request', function(method, path, params) {
          expect(method).to.equal('GET');
          expect(path).to.be.equal('travel/country');
          done();
        });
        var result  = client.get();
    });

    it('Should call _request with the good parameters when there is a parameter', function(done) {
        var client = request('travel/country');
        sandbox.stub(client, '_request', function(method, path, params) {
          expect(method).to.equal('GET');
          expect(path).to.equal('travel/country/EU');
          expect(params).to.be.undefined;
          done();
        });
        var result  = client.get('EU');
    });

    it('Should call _request with the good parameters when there is two  parameter', function(done) {
        var client = request('travel/country');
        sandbox.stub(client, '_request', function(method, path, params) {
          expect(method).to.equal('GET');
          expect(path).to.equal('travel/country/EU/test');
          expect(params).to.be.undefined;
          done();
        });
        var result  = client.get('EU', 'test');
    });
  });

  describe('#post()', function() {

    var sandbox;

    beforeEach(function(done) {
      sandbox = sinon.sandbox.create();
      done();
    });

    afterEach(function(done) {
      sandbox.restore();
      done();
    });




    it('Should call _request with the good parameters when there is no parameters', function(done) {
        var client = request('travel/country');
        sandbox.stub(client, '_request', function(method, path, params) {
          expect(method).to.equal('POST');
          expect(path).to.equal('travel/country');
          expect(params).to.be.undefined;
          done();
        });
        var result  = client.post();
    });

    it('Should call _request with the good parameters when there is a parameters', function(done) {
        var client = request('travel/country');
        sandbox.stub(client, '_request', function(method, path, params) {
          expect(method).to.equal('POST');
          expect(params).to.be.deep.equal({id:1});
          done();
        });
        var result  = client.post({id: 1});
    });

  });

  describe('#_request()', function() {

    var sandbox;

    beforeEach(function(done) {
      sandbox = sinon.sandbox.create();
      done();
    });

    afterEach(function(done) {
      sandbox.restore();
      done();
    });

    it('Should reject the promise if method parameters is missing', function(done) {
        request('travel/country')
          ._request()
          .then(
            function() {},
            function(err) {
              expect(err).to.equal('Parameters is missing (method, path)');
              done();
          });
    });


    it('Should reject the promise if path parameters is missing', function(done) {
        var c = request('travel/country');
          c._request('GET')
          .then(
            function() {},
            function(err) {
              expect(err).to.equal('Parameters is missing (method, path)');
              done();
          });
    });

    it('Should call request with the good parameters on GET request', function(done) {
        var req = function(options) {
          expect(options.method).to.equal('GET');
          expect(options.url).to.equal('http://local.test/api/travel/country');
          expect(options.form).to.be.undefined;
          expect(options.json).to.be.true;
          done();
        };
        var client = request('travel/country', req)._request('GET', 'travel/country');
    });

    it('Should call request with the good parameters on POST request with data', function(done) {
        var req = function(options) {
          expect(options.method).to.equal('POST');
          expect(options.url).to.equal('http://local.test/api/travel/country');
          expect(options.form).to.deep.equal({id:1});
          expect(options.json).to.be.true;
          done();
        };
        var client = request('travel/country', req)._request('POST', 'travel/country', {id : 1});
    });


    it('Should resolve the promise if the request works well', function(done) {
        var req = function(options, fn) {
           var obj = {
               statusCode : 200,
               body : {
                 message : 'success'
               }
           };
           fn.apply(this,[undefined, obj]);
        };
        request('travel/country', req)
          ._request('GET', 'path/test')
          .then(function(response) {
            var expected = {
              message : 'success'
            };
            expect(response).to.deep.equal(expected);
            done();
          }).catch(done);
    });

    it('Should reject the promise if the request fail', function(done) {
        var req = function(options, fn) {
           fn.apply(this,['my Error']);
        };
        request('travel/country', req)
          ._request('GET', 'path/test')
          .then(function(){},
           function(err) {
            expect(err).to.equal('my Error');
            done();
          });
    });

  });
});

