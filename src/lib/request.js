var Promise = require('bluebird'),
  request = require('request');

var __request = function(apiRoute, req) {

  var _request = function(method, path, params) {

    var options = {
      method : method,
      url : [this.options.endpoint, path].join('/'),
      body : params,
      json : true,
      time : true,
      timeout : this.options.timeout,
      headers: {
        'Authorization': this.options.token
      }
    };
    console.log(options);
    return new Promise(function(resolve, reject) {
      if (!method || !path) return reject('Parameters is missing (method, path)');

      console.log('inside promise');


      (req || request)(options, function(err, response, body) {
        console.log('inside request', arguments);
        _log(options, err, response);
        if (err) return reject(err);
        if (200 !== response.statusCode) return reject(response.body);
        return resolve(response.body);
      });
    });
  }.bind(this);

  var _log = function(options, err, response) {
    var debug = process.env.NODE_DEBUG && /\bmw\b/.test(process.env.NODE_DEBUG);
    if (debug) {
      try {
         var l = {
           'URL' : options.url,
           'method' : options.method,
           'params' : options.form,
           'error-message' : err,
           '@version' : 1,
           '@timestamp' : Date.now(),
           'time-ms' : response && response.elapsedTime,
           'header' : response && response.headers,
           'node' : {
             'version' : process.version
           }
         };
         console.log(l);
      } catch (e) {
        console.log('Can\'t log', e, response);
      }
    }
  };

  return {
    get : function() {
      var args = Array.prototype.slice.call(arguments);
      var path = (args.length || []) && [apiRoute].concat(args).join('/');
      return this._request('GET', path);
    },
    post : function(params, route) {
      return this._request('POST', route || apiRoute, params);
    },
    put : function(params, route) {
      return this._request('PUT', route || apiRoute, params);
    },
    _request : _request
  };
};

module.exports = __request;
