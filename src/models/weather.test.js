const MW =  global.getMW('weather', require('./weather')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Weater', () => {


  it('weather', (done) => {
    nock(global.ENDPOINT)
    .get('/weather/2017-01-01')
    .reply(200, {
      description : 'sunny'
    });

    MW.weather('2017-01-01')
      .then( res => {
        expect(res.description).to.equal('sunny');
        done();
      }).catch(done);
  });
});




