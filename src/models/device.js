module.exports = function(){
  var _req = this.__request('device');
  return {
    get : function() {
      return _req.get();
    },
    create : function(data) {
      return _req.post(data);
    },
    update : function(id, data) {
      return _req.put(data, 'device/'+id);
    }
  }
};
