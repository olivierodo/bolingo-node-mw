const MW =  global.getMW('todo', require('./todo')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Todo', () => {


  it('get', (done) => {
    nock(global.ENDPOINT)
    .get('/todo/123')
    .reply(200, {
      'name' : 'my todo list'
    });

    MW.todo()
      .get(123)
      .then( res => {
        expect(res.name).to.equal('my todo list');
        done();
      });
  });

  it('list', (done) => {
    nock(global.ENDPOINT)
    .get('/todo')
    .reply(200, [{
      'name' : 'my todo list'
    }]);

    MW.todo()
      .list()
      .then( res => {
        expect(res.length).to.equal(1);
        expect(res[0].name).to.equal('my todo list');
        done();
      });
  });

  it('addTasks', (done) => {
    var tasks = [{
      name : 'post Test'
    }];

    nock(global.ENDPOINT)
    .post('/todo/123', tasks => true)
    .reply(200, {
      success : true
    });
    MW.todo()
      .addTasks(123, tasks)
      .then( res => {
        expect(res.success).to.true;
        done();
      })
      .catch( done);
  });
});
