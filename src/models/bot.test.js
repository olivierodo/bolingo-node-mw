const MW =  global.getMW('bot', require('./bot')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Bot', () => {


  it('talk', (done) => {
    var data = {
      text : 'this is my test',
      uid : '12345',
      name : 'post Test'
    };

    nock(global.ENDPOINT)
    .post('/bot/talk', data)
    .reply(200, {
      'Good' : 1
    });
    MW.bot()
      .talk(data.text, data.uid, {name : data.name})
      .then( res => {
        expect(res.Good).to.equal(1);
        done();
      })
      .catch( done);
  });
});

