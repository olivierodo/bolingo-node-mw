module.exports = function(){
  var _req = this.__request('pictures');
  return {
    get : function() {
      return _req.get();
    },
    create : function(data) {
      return _req.post(data);
    }
  }
};
