const MW =  global.getMW('note', require('./note')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Note', () => {


  it('get', (done) => {
    nock(global.ENDPOINT)
    .get('/note')
    .reply(200, [{
      'name' : 'my note'
    }]);

    MW.note()
      .get()
      .then( res => {
        expect(res.length).to.equal(1);
        expect(res[0].name).to.equal('my note');
        done();
      });
  });

  it('create', (done) => {
    var data = {
      name : 'post Test'
    };

    nock(global.ENDPOINT)
    .post('/note', data)
    .reply(200, {
      'name' : data.name
    });
    MW.note()
      .create(data)
      .then( res => {
        expect(res.name).to.equal(data.name);
        done();
      })
      .catch( done);
  });
});


