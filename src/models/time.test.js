const MW =  global.getMW('time', require('./time')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Time', () => {


  it('time', (done) => {
    nock(global.ENDPOINT)
    .get('/time')
    .reply(200, {
      time : '5:40 PM'
    });

    MW.time()
      .then( res => {
        expect(res.time).to.equal('5:40 PM');
        done();
      }).catch(done);
  });
});




