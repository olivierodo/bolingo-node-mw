const MW =  global.getMW('device', require('./device')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Device', () => {


  it('get', (done) => {
    nock(global.ENDPOINT)
    .get('/device')
    .reply(200, [{
      'name' : 'my device'
    }]);

    MW.device()
      .get()
      .then( res => {
        expect(res.length).to.equal(1);
        expect(res[0].name).to.equal('my device');
        done();
      });
  });

  it('create', (done) => {
    var data = {
      name : 'post Test'
    };

    nock(global.ENDPOINT)
    .post('/device', data)
    .reply(200, {
      'name' : data.name
    });
    MW.device()
      .create(data)
      .then( res => {
        expect(res.name).to.equal(data.name);
        done();
      })
      .catch( done);
  });

  it('update', (done) => {
    var data = {
      name : 'put Test'
    };

    nock(global.ENDPOINT)
    .put('/device/123', data)
    .reply(200, {
      'name' : data.name
    });
    MW.device()
      .update(123, data)
      .then( res => {
        expect(res.name).to.equal(data.name);
        done();
      })
      .catch( done);
  });
});

