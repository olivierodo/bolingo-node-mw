module.exports = function(){
  var _req = this.__request('bot');
  return {
    talk : function(text, uid, data) {
      data = data || {};
      data.text = text;
      data.uid = uid;
      return _req.post(data,'bot/talk');
    }
  }
};
