const MW =  global.getMW('picture', require('./picture')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Picture', () => {


  it('get', (done) => {
    nock(global.ENDPOINT)
    .get('/pictures')
    .reply(200, [{
      'name' : 'my picture'
    }]);

    MW.picture()
      .get()
      .then( res => {
        expect(res.length).to.equal(1);
        expect(res[0].name).to.equal('my picture');
        done();
      });
  });

  it('create', (done) => {
    var data = {
      name : 'post Test'
    };

    nock(global.ENDPOINT)
    .post('/pictures', data)
    .reply(200, {
      'name' : data.name
    });
    MW.picture()
      .create(data)
      .then( res => {
        expect(res.name).to.equal(data.name);
        done();
      })
      .catch( done);
  });

});

