module.exports = function(){
  var _req = this.__request('fish');
  return {
    feed : function() {
      return _req.put({}, 'fish/feed');
    },
    get : function() {
      return _req.get();
    }
  }
};

