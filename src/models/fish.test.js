const MW =  global.getMW('fish', require('./fish')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Fish', () => {


  it('feed', (done) => {
    nock(global.ENDPOINT)
    .put('/fish/feed')
    .reply(200, {
      'success' : true
    });

    MW.fish()
      .feed()
      .then( res => {
        expect(res.success).to.true;
        done();
      });
  });

  it('get', (done) => {
    nock(global.ENDPOINT)
    .get('/fish')
    .reply(200, [{
      'name' : 'my fish activity'
    }]);

    MW.fish()
      .get()
      .then( res => {
        expect(res.length).to.equal(1);
        expect(res[0].name).to.equal('my fish activity');
        done();
      });
  });

});


