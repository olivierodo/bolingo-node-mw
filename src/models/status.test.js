const MW =  global.getMW('status', require('./status')),
  expect = require('chai').expect,
  sinon = require('sinon'),
  nock = require('nock');

describe('Status', () => {


  it('status', (done) => {
    nock(global.ENDPOINT)
    .get('/status')
    .reply(200, {
      version : 5.4
    });

    MW.status()
      .then( res => {
        expect(res.version).to.equal(5.4);
        done();
      }).catch(done);
  });
});



