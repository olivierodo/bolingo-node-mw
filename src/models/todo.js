
module.exports = function(){
  var _req = this.__request('todo');
  return {
    list : function() {
      return _req.get();
    },
    get : function(id) {
      return _req.get(id);
    },
    addTasks : function(idList, tasks) {
      return _req.post(tasks, ['todo', idList].join('/'));
    }
  }
};
