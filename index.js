'use strict';

// Package version
var VERSION = require('./package.json').version;

var MW = function(options) {
  if (!(this instanceof MW)) { return new MW(options) }

  this.options = {
    endpoint : (options && options.endpoint) || process.env.MW_URL,
    token : (options && options.token) || process.env.MW_TOKEN,
    timeout : (options && options.timeout) || process.env.MW_TIMEOUT,
    version : VERSION
  };
};

MW.prototype.__request = require('./src/lib/request.js');
MW.prototype.status = require('./src/models/status.js');
MW.prototype.time = require('./src/models/time.js');
MW.prototype.weather = require('./src/models/weather.js');
MW.prototype.note = require('./src/models/note.js');
MW.prototype.todo = require('./src/models/todo.js');
MW.prototype.device = require('./src/models/device.js');
MW.prototype.fish = require('./src/models/fish.js');
MW.prototype.bot = require('./src/models/bot.js');
MW.prototype.picture = require('./src/models/picture.js');

module.exports = MW;
